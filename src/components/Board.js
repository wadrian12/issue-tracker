
import styled from 'styled-components';
import React, { Component } from 'react';
import { DragDropContext } from 'react-beautiful-dnd';
import { getItems, reorderColumn, isTaskTransitionAllowed, moveBetweenColumns } from '../utils';

import Column from './Column';

const StyledBoard = styled.div`
  display: flex;
`;

export default class Board extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startColumn: null,
      columns: {
        open: { tasksList: getItems(5, 0, 'open'), allowedDestinations: ['pending', 'closed'] },
        pending: { tasksList: getItems(5, 10, 'pending'), allowedDestinations: ['closed'] },
        closed: { tasksList: getItems(5, 20, 'closed'), allowedDestinations: [] }
      }
    };
    this.onDragStart = this.onDragStart.bind(this);
    this.onDragEnd = this.onDragEnd.bind(this);
    this.getAllowedDestinations = this.getAllowedDestinations.bind(this);
  }

  onDragStart (start) {
    this.setState({
      startColumn: start.source.droppableId
    });
  }

  onDragEnd (result) {
    this.setState({ startColumn: null });

    if (!result.destination) {
      return;
    }

    if (result.source.droppableId === result.destination.droppableId) {
      this.setState({
        columns: {
          ...this.state.columns,
          [result.source.droppableId]: reorderColumn(this.state.columns[result.source.droppableId], result.source, result.destination)
        }
      });

      return;
    }

    this.setState({
      columns: {
        ...this.state.columns,
        ...moveBetweenColumns(this.state.columns, result.source, result.destination)
      }
    });
  }

  getAllowedDestinations () {
    return this.state.columns[this.state.startColumn].allowedDestinations;
  }

  render() {
    const checkAllowed = (destination) => this.state.startColumn !== null ?
      isTaskTransitionAllowed(this.state.startColumn, destination, this.getAllowedDestinations()) :
      false;

    return (
      <StyledBoard>
        <DragDropContext onDragStart={this.onDragStart} onDragEnd={this.onDragEnd}>
          { Object.keys(this.state.columns).map((columnName, index) => (
            <Column
              key={`${columnName}-${index}`}
              id={columnName}
              tasksList={this.state.columns[columnName].tasksList}
              isDropDisabled={!checkAllowed(columnName)}
            />
          )) }
        </DragDropContext>
      </StyledBoard>
    );
  }
}