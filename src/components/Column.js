import React from 'react';
import styled from 'styled-components';
import { Droppable } from 'react-beautiful-dnd';

import Task from './Task';

const StyledColumn = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  background: ${props => props.isDraggingOver ? 'lightblue' : '#d3d3d35e'};
  padding: 0 8px 8px;
  margin: 5px;
  width: 250px;
`;

const ColumnName = styled.h3`
  margin: 10px 0;
  text-align: center;
`;

const Column = (props) => (
  <Droppable droppableId={props.id} isDropDisabled={props.isDropDisabled}>
    {(provided, snapshot) => (
      <StyledColumn
        ref={provided.innerRef}
        isDraggingOver={snapshot.isDraggingOver}
      >
        <ColumnName>{props.id.toUpperCase()}</ColumnName>
        {props.tasksList.map((item, index) => (
          <Task key={item.id} item={item} index={index} />
        ))}
        {provided.placeholder}
      </StyledColumn>
    )}
  </Droppable>
);

export default Column;
