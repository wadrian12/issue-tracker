import React from 'react';
import styled from 'styled-components';
import { Draggable } from 'react-beautiful-dnd';

const StyledTask = styled.div`
  userSelect: 'none';
  padding: 10px;
  margin: 0 0 5px;
  background: ${props => props.isDragging ? 'lightgreen' : '#80808063'};
`;

const TaskTitle = styled.h4`
  margin: 5px 0 2.5px;
`;

const TaskDescription = styled.p`
  margin: 7.5px 0;
  text-align: justify;
`;

const Task = ({ item, index }) => (
  <Draggable draggableId={item.id} index={index}>
    {(provided, snapshot) => (
      <StyledTask
        ref={provided.innerRef}
        isDragging={snapshot.isDragging}
        style={provided.draggableProps.style}
        {...provided.draggableProps}
        {...provided.dragHandleProps}
      >
        <TaskTitle>{item.content.title}</TaskTitle>
        <TaskDescription>{item.content.description}</TaskDescription>
      </StyledTask>
    )}
  </Draggable>
);

export default Task;
