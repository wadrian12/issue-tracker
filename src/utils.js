import { loremIpsum } from 'lorem-ipsum';

export const moveBetweenColumns = (columns, source, destination) => {
  const columnsCopy = JSON.parse(JSON.stringify(columns));
  const sourceColumn = columnsCopy[source.droppableId] || null;
  const destinationColumn = columnsCopy[destination.droppableId] || { tasksList: [] };

  if (sourceColumn === null) {
    throw new Error('Source column does not exist.');
  }

  const [ removedValue = {} ] = sourceColumn.tasksList.splice(source.index, 1);
  destinationColumn.tasksList.splice(destination.index, 0, {
    ...removedValue,
    content: {
      ...removedValue.content,
      status: destination.droppableId
    }
  });

  return {
    [source.droppableId]: sourceColumn,
    [destination.droppableId]: destinationColumn
  }
}

export const reorderColumn = (column, source, destination) => {
  if (!column || (column.tasksList && column.tasksList.length === 0)) {
    throw new Error('Column to reorder cannot be empty.');
  }

  if (source.droppableId !== destination.droppableId) {
    throw new Error('Source cannot be different than destination.');
  }

  const columnCopy = JSON.parse(JSON.stringify(column));
  const [ removedValue ] = columnCopy.tasksList.splice(source.index, 1);
  columnCopy.tasksList.splice(destination.index, 0, removedValue);

  return columnCopy;
};

export const isTaskTransitionAllowed = (sourceName, destinationName, allowedDestinations) =>
  sourceName === destinationName || allowedDestinations.includes(destinationName);

export const getItems = (count, offset = 0, status) =>
  Array.from({ length: count }, (v, k) => k).map(k => ({
    id: `task-${k + offset}`,
    content: {
      title: `TASK-${k + offset}`,
      description: loremIpsum(),
      status: status
    }
  }));

