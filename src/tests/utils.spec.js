import { moveBetweenColumns, reorderColumn, isTaskTransitionAllowed } from '../utils';

describe('moveBetweenColumns', () => {
  it('correctly move task between columns', () => {
    const columns = {
      foo: { tasksList: [
        { id: 'foo-task-1', content: { status: 'foo' } },
        { id: 'foo-task-2', content: { status: 'foo' } }
      ] },
      bar: { tasksList: [
        { id: 'bar-task-1', content: { status: 'bar' } },
        { id: 'bar-task-2', content: { status: 'bar' } }
      ] }
    }
    const source = { droppableId: 'foo', index: 1 };
    const destination = { droppableId: 'bar', index: 2 };

    const result = moveBetweenColumns(columns, source, destination)

    expect(result).toHaveProperty('foo', {
      tasksList: [ { id: 'foo-task-1', content: { status: 'foo' } } ]
    });
    expect(result).toHaveProperty('bar', {
      tasksList: [
        { id: 'bar-task-1', content: { status: 'bar' } },
        { id: 'bar-task-2', content: { status: 'bar' } },
        { id: 'foo-task-2', content: { status: 'bar' } }
      ]
    });
  });

  it('change task status', () => {
    const columns = {
      foo: { tasksList: [
        { id: 'foo-task-1', content: { status: 'foo' } }
      ] },
      bar: { tasksList: [] }
    }
    const source = { droppableId: 'foo', index: 0 };
    const destination = { droppableId: 'bar', index: 1 };

    const result = moveBetweenColumns(columns, source, destination)
    const task = result.bar.tasksList[0];

    expect(task).toHaveProperty('content.status', 'bar');
  });

  it('add destination column when it does not exist', () => {
    const columns = {
      foo: { tasksList: [
        { id: 'foo-task-1', content: { status: 'foo' } },
        { id: 'foo-task-2', content: { status: 'foo' } }
      ] }
    }
    const source = { droppableId: 'foo', index: 1 };
    const destination = { droppableId: 'bar', index: 2 };

    const result = moveBetweenColumns(columns, source, destination)

    expect(result).toHaveProperty('foo', {
      tasksList: [ { id: 'foo-task-1', content: { status: 'foo' } } ]
    });
    expect(result).toHaveProperty('bar', {
      tasksList: [
        { id: 'foo-task-2', content: { status: 'bar' } }
      ]
    });
  });

  it('throw an error when source column does not exist', () => {
    const columns = {
      bar: { tasksList: [] }
    }
    const source = { droppableId: 'foo', index: 1 };
    const destination = { droppableId: 'bar', index: 2 };

    try {
      const result = moveBetweenColumns(columns, source, destination)
    } catch (error) {
      expect(error).toHaveProperty('message', 'Source column does not exist.');
    }
  });
});

describe('reorderColumn', () => {
  it('return reordered column', () => {
    const column = {
      tasksList: [
        { id: 'foo-task-1', content: { status: 'foo' } },
        { id: 'foo-task-2', content: { status: 'foo' } },
        { id: 'foo-task-3', content: { status: 'foo' } }
      ]
    };
    const source = { droppableId: 'foo', index: 1 };
    const destination = { droppableId: 'foo', index: 2 };

    const result = reorderColumn(column, source, destination);
    
    expect(result).toHaveProperty('tasksList');
    expect(result.tasksList[0]).toHaveProperty('id', 'foo-task-1')
    expect(result.tasksList[1]).toHaveProperty('id', 'foo-task-3')
    expect(result.tasksList[2]).toHaveProperty('id', 'foo-task-2')
  });

  it('throw an error when column does not exist', () => {
    const source = { droppableId: 'foo', index: 1 };
    const destination = { droppableId: 'bar', index: 2 };

    try {
      const result = reorderColumn(undefined, source, destination);
    } catch (error) {
      expect(error).toHaveProperty('message', 'Column to reorder cannot be empty.')
    }
  });

  it('throw an error when column is empty', () => {
    const column = {
      tasksList: []
    };

    const source = { droppableId: 'foo', index: 1 };
    const destination = { droppableId: 'bar', index: 2 };

    try {
      const result = reorderColumn(column, source, destination);
    } catch (error) {
      expect(error).toHaveProperty('message', 'Column to reorder cannot be empty.')
    }
  });

  it('throw an error when source is different than destination', () => {
    const column = {
      tasksList: [
        { id: 'foo-task-1', content: { status: 'foo' } }
      ]
    };

    const source = { droppableId: 'foo', index: 1 };
    const destination = { droppableId: 'bar', index: 2 };

    try {
      const result = reorderColumn(column, source, destination);
    } catch (error) {
      expect(error).toHaveProperty('message', 'Source cannot be different than destination.')
    }
  });
});

describe('isTaskTransitionAllowed', () => {
  it('allowed transition in same direction', () => {
    const sourceName = 'foo';
    const destinationName = 'foo';
    const allowedDestinations = [];

    const result = isTaskTransitionAllowed(sourceName, destinationName, allowedDestinations);

    expect(result).toEqual(true);
  });

  it('allow transition to one of allowed destinations', () => {
    const sourceName = 'foo';
    const destinationName = 'bar';
    const allowedDestinations = [ 'bar', 'foobar' ];

    const result = isTaskTransitionAllowed(sourceName, destinationName, allowedDestinations);

    expect(result).toEqual(true);
  });

  it('not allow transition to one of not allowed destinations', () => {
    const sourceName = 'foo';
    const destinationName = 'bar';
    const allowedDestinations = [ 'foobar' ];

    const result = isTaskTransitionAllowed(sourceName, destinationName, allowedDestinations);

    expect(result).toEqual(false);
  });
});
